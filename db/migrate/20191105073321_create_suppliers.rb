class CreateSuppliers < ActiveRecord::Migration[6.0]
  def change
    create_table :suppliers do |t|
      t.string :phone
      t.string :name
      t.string :address
      t.string :password
      t.string :email
      t.string :city
    
      t.timestamps
    end
  end
end
