class CreateCouriers < ActiveRecord::Migration[6.0]
  def change
    create_table :couriers do |t|
      t.string :phone
      t.string :name
      t.string :address

      t.timestamps
    end
  end
end
