class AuthenticationController < ApplicationController
skip_before_action :verify_authenticity_token
before_action :authorize_request, except: :login

  # POST /auth/login
  require "json_web_token"
  def login
    @user = Customer.find_by_email(params[:email])
    if @user&.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      time = Time.now + 24.hours.to_i
      render json: {token: token, message: "You have login Successfully"}, status: :ok
    else
      render json: { message: "Email or password is incorrect" }, status: :unauthorized
    end
  end

  private

  def login_params
    params.permit(:email, :password)
  end

end
